const
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    clean = require('gulp-clean'),
    gulpSequence = require('gulp-sequence'),
    sass = require('gulp-sass');

let env = 'prod';

/**
 * Config for path variables. E.g. differentiate between src and dist folder.
 */
let path = {
    // All source files reside inside the src folder.
    src: './src/',

    // All files ready for production are being copied / compiled into the distribution folder
    dist: './dist/',

    // All css files.
    css: "/**/*.sass",

    // All html files.
    html: "/**/*.html"
};

/**
 * This task compiles css for production use by prefixing for cross browser compatibility, minifying and
 * creating source maps.
 */
gulp.task('sass', function () {
    return gulp
    // Start a gulp stream for all css files inside the src folder
        .src(path.src + path.css)
        // Initialize the source maps
        .pipe(sourcemaps.init())
        //sass task
        .pipe(sass({
            includePaths: ["node_modules/"]
        }).on('error', sass.logError))
        // Prefix all css properties for all browsers
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        // Miniy the css.
        .pipe(cssnano())
        // Create the source maps in the same folder as the css file is compiled to.
        .pipe(sourcemaps.write("."))
        // Define the output folder (distribution).
        .pipe(gulp.dest(path.dist))
        // Reload the connect server for live reloading in the browser.
        .pipe(connect.reload());
});

/**
 * This task moves all html files from the src folder to the distribution folder.
 * Since no additional plugins are provided between src and gulp.dest the files are only being copied.
 */
gulp.task('moveHTML', () => {
    return gulp
    // Start a gulp stream for all html files inside the src folder
        .src(path.src + path.html)
        // Define the output folder (distribution)
        .pipe(gulp.dest(path.dist))
        // Reload the connect server for live reloading in the browser.
        .pipe(connect.reload())
});

/**
 * This task starts a small connect server, which is able to live reload after css, js or html changes.
 * Server will be running on localhost:1337.
 */
gulp.task('connect', () => {
    return connect.server({
        // Server root is the distribution folder.
        root: './dist',
        port: 1337,
        livereload: true
    })
});

/**
 * This task clears the distribution folder, removing old files which are not needed anymore.
 */
gulp.task('clean', () => {
    return gulp.src(path.dist)
        .pipe(clean())
});

/**
 * This task watches all css files inside the src folder and re-executes the css task.
 */
gulp.task('watch:sass', () => {
    gulp.watch(path.src + path.css, ['sass']);
});

/**
 * This task watches all html files inside the src folder and re-executes the moveHTML task.
 */
gulp.task('watch:html', () => {
    gulp.watch(path.src + path.html, ['moveHTML']);
});

/**
 * Default Task. This task can be started by executing gulp inside this directory without providing a task name.
 * Uses Gulp-Sequence to executes all necessary tasks in a given sequential order.
 */
gulp.task('default', (cb) => {
    gulpSequence(
        // Clear the distribution folder
        'clean',
        // Start the connect server
        'connect',
        // Move html files and compile css
        ['moveHTML', 'sass'],
        // Start watchers
        ['watch:sass', 'watch:html'],

        // IMPORTANT! Always pass the callback here. Otherwise Gulp-Sequence is not working correctly.
        cb
    )
});


